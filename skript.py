#!/usr/bin/sh
import threading
import serial
import os
import time
import re
import subprocess
import bluetooth

from rgb_led import rbgGreen, rbgBlinkYellow, rbgBlinkBlue
from database_access import getUsername
from database_access import withdraw

gpslist = ['57.46722200', '14.09816700'] #'57.46722200', '14.09816700'

time.sleep(5)

def startFona():
            subprocess.call("sudo pon fona", shell=True)
            print("Fona ON")
            rbgBlinkYellow()
            time.sleep(2)
            

def stopFona():
            subprocess.call("sudo poff fona", shell=True)
            print("Fona OFF")
            rbgBlinkYellow()
            time.sleep(2)
            
            
def read_GPS_Coord():
    os.system('sudo chmod 777 /dev/serial0')
    time.sleep(3)
    
    ser = serial.Serial('/dev/serial0', 115200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=1)

    ser.write(b'AT+CGNSPWR=1\n')
    time.sleep(5)
    ser.write(b'AT+CGPSINF=0\n')

    rcv=ser.read(100).strip()
    split=rcv.decode('ascii').split(',')
    
    #['5746.722200', '1409.816700']

    coor = split[1:3]
    
    xCoord = coor[0]
    xCoord = xCoord[0:2] + "." + xCoord[2:4] + xCoord[5:]
    
    yCoord = coor[1]
    yCoord = yCoord[0:2] + "." + yCoord[2:4] + yCoord[5:]
    
    ser.write(b'AT+CGNSPWR=0\n')

    return([xCoord, yCoord])


def gpsANDfona():
    #läsa kordinate

    global gpslist
    while (gpslist == [] or gpslist == ['0..00000', '0..00000']):    
        stopFona()
        time.sleep(2)
        print(gpslist)
        startFona()
        time.sleep(2)
        
    if gpslist != [] or gpslist != ['0..00000', '0..00000']:
        print("found gps")
        rbgGreen()
        time.sleep(2)
    
  
    
def bth_macAddr_search():
   while True:
        
        mac_addr_list=[]
        nearby_devices = bluetooth.discover_devices()
        print("found %d devices" % len(nearby_devices))
        rbgBlinkBlue()
        rbgGreen()
        for addr in nearby_devices:
            if not addr:
                continue
            print("Address: " + addr)
            userName = getUsername(addr)
            if userName:
              withdraw(gpslist[0], gpslist[1], userName)
              print("User name: " + userName)
        

def gps_print():
    mylist=read_GPS_Coord()
    for p in mylist:
        print(p)

def macAdrr_print():
    mac_addr =[]
    mac_addr= bth_macAddr_search()
    print(" %s" %(mac_addr))

#time
#tid= time.time()



#Created the Threads
t1= threading.Thread(target=gpsANDfona, args=())
t2= threading.Thread(target=bth_macAddr_search, args=())


#started the Threads

rbgGreen()
t1.start()
t1.join()


t2.start()
t2.join()


