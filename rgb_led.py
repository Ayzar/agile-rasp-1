import RPi.GPIO as GPIO
import time, sys


colour_off=[1,1,1]            #off
colour_white=[0,0,0]          #000 #vit
colour_yellow=[0,0,1]         #001 #gul
colour_purple=[0,1,0]         #010 #lila
colour_lightGreen=[1,0,0]     #100 #blå
colour_darkBlue=[1,1,0]       #110 #mörk blå
colour_red=[0,1,1]            #011 #röd
colour_green=[1,0,1]          #101 #grön
 
def rbg_Setcolor( color_num ):
    GPIO.setmode(GPIO.BOARD)
    RBG = [11,12,13]
    for pin in RBG:
        GPIO.setwarnings(False)
        GPIO.setup(pin,GPIO.OUT)   # Set pins' mode is output
        GPIO.output(pin,GPIO.HIGH)
        

    while True:
        if(len(color_num) == 3):
                    GPIO.output(11, int(color_num[0]))
                    GPIO.output(12, int(color_num[1]))
                    GPIO.output(13, int(color_num[2]))
                    break;
        elif(len(color_num) == [1,1,1]):
             GPIO.cleanup()
             break;
        else:
            GPIO.cleanup()
            break;
    return        
def rbgBlinkBlue():
    rbg_Setcolor(colour_darkBlue)
    time.sleep(1)
    rbg_Setcolor(colour_off)
    return

def rbgBlinkYellow():
    rbg_Setcolor(colour_off)
    time.sleep(1)
    rbg_Setcolor(colour_yellow)
    return

def rbgGreen():
    rbg_Setcolor(colour_green)
    return
            

    
