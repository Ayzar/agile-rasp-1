import json

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

import datetime as DateTime
import calendar
import time

# Use the application default credentials
cred = credentials.Certificate('viken-passage-314a4f4b96b6.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

EXPIRY_DATE = "expiryDate"


# Constants to set up the program
COST_TO_PASS = 150
FREE_PASS_TIME_IN_SEC = 3600
UTC_TIME_DIFF = calendar.timegm(time.localtime()) - calendar.timegm(time.gmtime())

def writeToHistory(location, USER_NAME, currDateTime):
    db.collection(u'Users').document(USER_NAME).collection(u"history").add({
        u'payment': COST_TO_PASS,
        u'date': currDateTime,
        u'position': location
    })

def withdraw(latitude, longitude, USER_NAME):

    location = firebase_admin.firestore.GeoPoint(float(latitude), float(longitude))

    transaction = db.transaction()
    doc_ref = db.collection(u'Users').document(USER_NAME)

    @firestore.transactional
    def update_in_transaction(transaction, doc_ref):

        snapshot = doc_ref.get(transaction = transaction)
        snapshotDict = snapshot.to_dict()
        expiryDateGMTime = snapshotDict[EXPIRY_DATE]

        currGMTime = DateTime.datetime.now() - DateTime.timedelta(seconds = UTC_TIME_DIFF)
        expiryDateGMTime = DateTime.datetime(
            expiryDateGMTime.year, 
            expiryDateGMTime.month, 
            expiryDateGMTime.day, 
            expiryDateGMTime.hour, 
            expiryDateGMTime.minute,
            expiryDateGMTime.second)

        if currGMTime < expiryDateGMTime:
            return

        transaction.update(doc_ref, {
            u'balance': snapshot.get(u'balance') - COST_TO_PASS,
            EXPIRY_DATE: currGMTime + DateTime.timedelta(seconds = FREE_PASS_TIME_IN_SEC)
        })
        writeToHistory(location, USER_NAME, currGMTime)

    update_in_transaction(transaction, doc_ref)

def getUsername(MAC_ADRESS):

    doc_ref = db.collection(u'Mac Addresses').document(MAC_ADRESS)

    try:
        doc = doc_ref.get()
        data = doc.to_dict()
        if data:
            return data["User Email"]
        else:
            raise ""
    except ValueError:
        return ""

userName = getUsername("5C:51:81:4C:2A:2C")
if userName:
    withdraw("57.777942", "14.158095", userName)
