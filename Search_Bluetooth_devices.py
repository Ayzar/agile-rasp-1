import bluetooth
import time


print("searching devices")

def bth_macAddr_search():
    nearby_devices = bluetooth.discover_devices(duration =1, flush_cache=True)
    print("found %d devices" % len(nearby_devices))

    for addr in nearby_devices:
        try:
            print(" %s" %(addr))
        except UnicodeEncodeError:
            print(" %s" %(addr.encode('uft-8', 'replace')))

while True:
    bth_macAddr_search()
    time.sleep(1)