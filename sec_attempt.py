import bluetooth

#print("searching devices")

nearby_devices = bluetooth.discover_devices(duration =3,flush_cache=True)
print("found %d devices" % len(nearby_devices))

for addr in nearby_devices:
    try:
        print(" %s" %(addr))
    except UnicodeEncodeError:
        print(" %s" %(addr))
